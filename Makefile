CC = gcc
CFLAGS = -std=c99 -O2 -Wall
Q ?= @
PROGRAMS = input strdemo
DIRS = lib
RM = rm -f

all: | $(DIRS) $(PROGRAMS)

$(DIRS): 
	mkdir $(DIRS)

strdemo: lib/strutils.so strdemo.o
	$(CC) $(CFLAGS) -L. -lstrutils -o strdemo strdemo.o

input: linkedlist.o inputsorted.o
	$(CC) $(CFLAGS) -o input linkedlist.o inputsorted.o

lib/strutils.so: strutils.o
	$(CC) -shared -o $@ $<

strutils.o: src/strutils.c
	$(CC) -fPIC -o $@ -c $<

%.o: src/%.c
	@echo "CC $<"
	$(Q)$(CC) $(CFLAGS) -c $<

clean:
	@echo "Removing objects and libraries"
	$(RM) *.o 
	$(RM) -r $(DIRS)
	$(RM) $(PROGRAMS)
