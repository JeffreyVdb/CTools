#include "linkedlist.h"

int main(int argc, char ** argv)
{
    // Ask the user to input stuff
    char key[100];
    char val[100];
    d_LL * list = NULL;

    // Keep count
    int numElements = 0;
    
    for (;;) {
	printf("Enter english word: ");
	if (!fgets(key, 100, stdin)) {
	    puts("Nothing entered");
	    break;
	}

	printf("Enter dutch translation: ");
	if (!fgets(val, 100, stdin)) {
	    puts("Nothing entered");
	    break;
	}

	// Remove newline characters from string	
	int n = 0;
	char * pkey = 0;
	char * pval = 0;
	
	if ((n = strlen(key)) > 0) {
	    key[n-1] = '\0';
	    pkey = (char *) malloc(n + 1);
	    strcpy(pkey, key);
	    pkey[n] = '\0';
	}
	    
	if ((n = strlen(val)) > 0) {
	    val[n-1] = '\0';
	    pval = (char *) malloc(n + 1);
	    strcpy(pval, val);
	    pval[n] = '\0';
	}
	
	printf("Adding %s: %s\n\n", key,val);
	
	// Add to linkedlist
	if (numElements < 1) {
	    list = add_to_ll(NULL, pkey, pval, O_SORT);
	}
	else {
	    add_to_ll(list, pkey, pval, O_SORT);
	}

	numElements++;
    }

    // Rewind list to start
    while (list->previous)
	list = list->previous;

    d_LL * list_start = list;

    // Print list if contains elements
    puts ("\nSorted List:\n");    
    while (list) {
	printf("%20s: %s\n",
	       list->key,
	       (char *) list->value);

	list = list->next;
    }

    // Free memory ...
    puts("\n\nDEBUG");
    for (;;) {
	d_LL * to_free = NULL;
	
	if (list_start->next) {
	    list_start = list_start->next;
	    if (!list_start->previous)
		break;

	    to_free = list_start->previous;
	    printf("Freeing: %s\n", to_free->key);
	    
	    free(to_free->key);
	    free(to_free->value);
	    free(to_free);
	}
	else {
	    to_free = list_start;
	    printf("Freeing: %s\n", to_free->key);
	    
	    free(to_free->key);
	    free(to_free->value);
	    free(to_free);
	    break;
	}
    }
    
    return 0;
}
