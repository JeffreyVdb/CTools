#ifndef _STRING_UTILS_H_
#define _STRING_UTILS_H_

#include <stdlib.h>

extern char * strtrim(char * str);
extern size_t strntrim(char * dest, size_t n, const char * str);

#endif // _STRING_UTILS_H_
