#include "strutils.h"
#include <string.h>
#include <ctype.h>

char * strtrim(char * str)
{
    char * end = 0;

    while (isspace(*str))
        str++;
    
    if (*str == '\0')
        return str;

    end = str + strlen(str) - 1;
    while (end > str && isspace(*end))
        end--;

    *(end + 1) = '\0';
    return str;
}

size_t strntrim(char * dest, size_t n, const char * str)
{
    size_t len = 0, tocopy = 0;
    const char * end = 0;
    while (isspace(*str))
        str++;
    
    if (*str == '\0')
        return (*dest = 0);

    end = str + strlen(str) - 1;
    while (end > str && isspace(*end))
        end--;

    tocopy = (n <= (len = strlen(str))) ? n - 1 : len;
    memcpy(dest, str, tocopy);
    *(dest + tocopy) = 0;

    return tocopy;
}
