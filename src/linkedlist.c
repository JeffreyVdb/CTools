#include "linkedlist.h"

struct d_LL * add_to_ll(d_LL * list, const char * key, const void * value, int opts)
{
    if (! list) {
	// Create new list
	d_LL * nList = (d_LL*) malloc(sizeof(d_LL));
	nList->previous = NULL;
	nList->next = NULL;
	nList->key = (char *) key;
	nList->value = (void *) value;
	nList->position = 0;

	return nList;
    }
    
    d_LL * list_ref = list;

    if (opts & O_SORT) {
	// Add element using alphabetical order
	for (;;) {
	    int cmp = strcmp(key, list_ref->key);
	    
	    if (cmp > 0) {
		// Key comes after current element, check if it is before next element
		if (!list_ref->next) {
		    return add_to_ll(list_ref, key, value, O_DIRECT_APPEND);
		}
		else {
		    if (strcmp(key, list_ref->next->key) < 0) {
			return add_to_ll(list_ref, key, value, O_DIRECT_APPEND);
		    }
		    else {
			list_ref = list_ref->next;
			continue;
		    }
		}
	    }
	    else {
		// Key is smaller, check if smaller then previous
		if (!list_ref->previous) {
		    return add_to_ll(list_ref, key, value, O_DIRECT_INSERT);
		}
		else {
		    if (strcmp(key, list_ref->previous->key) > 0) {
			return add_to_ll(list_ref, key, value, O_DIRECT_INSERT);
		    }
		    else {
			list_ref = list_ref->previous;
			continue;
		    }
		}
	    }
	}
    }

    d_LL * new_elem = (d_LL*) malloc(sizeof(d_LL));
    new_elem->value = (void *) value;
    new_elem->key = (char *) key;
    
    if (opts & O_DIRECT_APPEND) {
	d_LL * next_elem = list_ref->next;
	
	new_elem->next = next_elem;
	new_elem->previous = list_ref;
	new_elem->position = list_ref->position + 1;

	if (next_elem)
	    next_elem->previous = new_elem;

	// Add to list
	list_ref->next = new_elem;
    }
    else if (opts & O_DIRECT_INSERT) {
	d_LL * prev_elem = list_ref->previous;
	
	new_elem->next = list_ref;
	new_elem->previous = prev_elem;
	new_elem->position = list_ref->position - 1;
	
	if (prev_elem)
	    prev_elem->next = new_elem;

	// Add to list
	list_ref->previous = new_elem;
    }
    else {
	// Navigate to the end of the list...
	while (list_ref->next) {
	    list_ref = list_ref->next;
	}

	// Now add to end of the list..
	new_elem->previous = list_ref;
	new_elem->next = NULL;
	new_elem->position = list_ref->position + 1;

	// Add it..
	list_ref->next = new_elem;
    }

    return new_elem;;
}
