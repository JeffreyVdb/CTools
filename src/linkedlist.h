#ifndef _LINKEDLIST_H_
#define _LINKEDLIST_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define O_SORT (1)
#define O_BIN_SEARCH (2)
#define O_DIRECT_APPEND (4)
#define O_DIRECT_INSERT (8)

// Structure of the linkedlist
typedef struct d_LL
{
    struct d_LL * previous;
    int position;
    char * key;
    void * value;
    struct d_LL * next;
} d_LL;

// Add an element to the list
struct d_LL * add_to_ll(d_LL * list, const char * key, const void * value, int opts);

#endif // LINKEDLIST_H_
